package com.joy.common;

public abstract class Operation extends MessageBody{

    public abstract OperationResult execute();

}
