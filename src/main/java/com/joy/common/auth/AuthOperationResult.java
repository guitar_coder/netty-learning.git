package com.joy.common.auth;

import com.joy.common.OperationResult;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AuthOperationResult extends OperationResult {

    private final boolean passAuth;

}
