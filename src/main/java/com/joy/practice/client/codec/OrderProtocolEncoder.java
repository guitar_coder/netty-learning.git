package com.joy.practice.client.codec;

import com.joy.common.RequestMessage;
import com.joy.common.ResponseMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * 客户端协议编码，将requestMessage变成字节流
 * input:requestMessage
 * output:byteBuf
 */
public class OrderProtocolEncoder extends MessageToMessageEncoder<RequestMessage> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, RequestMessage requestMes, List<Object> list) throws Exception {
        ByteBuf buffer = channelHandlerContext.alloc().buffer();
        requestMes.encode(buffer);
        list.add(buffer);
    }
}
