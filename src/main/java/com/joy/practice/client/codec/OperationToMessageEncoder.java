package com.joy.practice.client.codec;

import com.joy.common.Operation;
import com.joy.common.RequestMessage;
import com.joy.util.IdUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * 将operation转换成requestMessage
 */
public class OperationToMessageEncoder extends MessageToMessageEncoder<Operation> {
    @Override
    protected void encode(ChannelHandlerContext ctx, Operation operation, List<Object> out) throws Exception {
        RequestMessage msg = new RequestMessage(IdUtil.nextId(), operation);
        out.add(msg);
    }
}
