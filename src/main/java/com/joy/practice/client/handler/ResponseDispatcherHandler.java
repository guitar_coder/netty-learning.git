package com.joy.practice.client.handler;

import com.joy.common.ResponseMessage;
import com.joy.practice.client.handler.dispatcher.PendingRequestHolder;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ResponseDispatcherHandler extends SimpleChannelInboundHandler<ResponseMessage> {

    private PendingRequestHolder pendingRequestHolder;

    public ResponseDispatcherHandler(PendingRequestHolder pendingRequestHolder) {
        this.pendingRequestHolder = pendingRequestHolder;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ResponseMessage msg) throws Exception {
        pendingRequestHolder.set(msg.getMessageHeader().getStreamId(), msg.getMessageBody());
    }
}
